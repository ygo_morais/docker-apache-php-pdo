# Docker - Apache, PHP e PDO

![Badge](https://img.shields.io/static/v1?label=apache&message=2.4.57&color=blue)
![Badge](https://img.shields.io/static/v1?label=php&message=8.2.12&color=blue)
![Badge](https://img.shields.io/static/v1?label=composer&message=2.6.5&color=blue)
![Badge](https://img.shields.io/static/v1?label=memcached&message=1.6.18&color=blue)
[![Badge](https://img.shields.io/static/v1?label=bitbucket&message=ygo_morais&color=green&logo=bitbucket)](https://bitbucket.com/ygo_morais/docker-apache-php-pdo)
![Badge](https://img.shields.io/static/v1?label=license&message=MIT&color=green)

## Sobre

Imagem para criação de containers de servidores web com Apache e PHP. Também possui drives de conexão com banco de dados em Mysql, Oracle, Postgres, SQL Server e Oracle com PDO.

## Modulos PHP

Lista de todos os módulos disponíveis nessa imagem:

- Core
- ctype
- curl
- date
- dom
- exif
- fileinfo
- filter
- ftp
- gd
- gettext
- hash
- iconv
- json
- libxml
- mbstring
- mcrypt
- memcached
- mysqlnd
- openssl
- pcre
- PDO
- pdo_dblib
- pdo_mysql
- PDO_OCI
- pdo_pgsql
- pdo_sqlite
- Phar
- posix
- random
- readline
- Reflection
- session
- SimpleXML
- sodium
- SPL
- sqlite3
- standard
- tokenizer
- xml
- xmlreader
- xmlwriter
- zip
- zlib

## Download da Imagem e Inicialização do Container

```bash
docker container run -itd --name <nome_container>  -p 80:80 -p 443:443 -v <diretorio_codigos>:/var/www/html -v <diretorio-logs>:/var/log/apache2 --restart=always ygoamaral/apache-php-pdo:latest
```

Exemplo

```bash
docker container run -itd --name meu-web-dev -p 80:80 -p 443:443 -v /home/www:/var/www/html -v /home/www/.logs-apache:/var/log/apache2 --restart=always ygoamaral/apache-php-pdo:latest
```

_Obs1: o parâmetro '--restart=always' é usado para que o container inicie junto com o sistema operacional (Docker host)._

_Obs2: para usar log, além de mapear o diretório de logs, deve-se habilitar o php a gravar logs em arquivos. Para isso, veja o tópico, [Configurações Adicionais](#configurações-adicionais)._

Em seguida digite em seu navegador: `http://localhost`. Se você visualizar seus scripts ou a página `403 Forbidden`, a instalação foi bem sucedida.

## Memcached

Para usar o serviço de cache, Memcached, deve-se inicia-lo:

```bash
docker exec -it <nome_container> service memcached start
```

## Configurações Adicionais

Primeiro, crie o arquivo de configuração, ".htaccess" na raiz de seu projeto ou em outro diretório necessário. Logo em seguida adicione as seguintes configurações opcionais nesse arquivo:

- Permissão para visualizar arquivos e diretórios direto do navegador:

```bash
Options +Indexes
```

- Aumentar o limite de upload de arquivos:

```bash
php_value memory_limit 100M
php_value post_max_size 100M
php_value upload_max_filesize 100M
```

- Habilitando gravação de logs em arquivos:

```bash
php_flag log_errors On
```
