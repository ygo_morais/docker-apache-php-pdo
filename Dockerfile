FROM php:8.2.12-apache

ENV LD_LIBRARY_PATH=/opt/oracle/instantclient_12_1 \
  ORACLE_HOME=/opt/oracle/instantclient_12_1

RUN mkdir /opt/oracle

COPY ./instantclient_12_1.tar.xz /opt/oracle

RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  wget \
  unzip \
  gnupg \
  apt-utils \
  memcached \
  libpq-dev \
  libaio-dev \
  libpng-dev \
  libzip-dev \
  freetds-dev \
  libonig-dev \
  libmcrypt-dev \
  libmemcached-dev \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  exiftool \
  && echo "deb http://apt.postgresql.org/pub/repos/apt buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  apt-key add - \
  && apt-get update \
  && apt-get install --no-install-recommends -y postgresql-server-dev-10 \
  && ln -sf /usr/share/zoneinfo/America/Fortaleza /etc/localtime \
  && cd /opt/oracle \
  && tar xf instantclient_12_1.tar.xz \
  && rm -f instantclient_12_1.tar.xz \
  && ln -s /opt/oracle/instantclient_12_1/libocci.so.12.1 /opt/oracle/instantclient_12_1/libocci.so \
  && ln -s /opt/oracle/instantclient_12_1/libclntsh.so.12.1 /opt/oracle/instantclient_12_1/libclntsh.so \
  && ln -s /opt/oracle/instantclient_12_1/libclntshcore.so.12.1 /opt/oracle/instantclient_12_1/libclntshcore.so \
  && pecl install mcrypt \
  && pecl install memcached \
  && docker-php-ext-configure pdo_oci --with-pdo-oci=instantclient,/opt/oracle/instantclient_12_1,12.1 \
  && docker-php-ext-configure pdo_dblib --with-libdir=/lib/x86_64-linux-gnu \
  && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
  && docker-php-ext-enable mcrypt \
  && docker-php-ext-enable memcached \
  && docker-php-ext-install -j$(nproc) \
  gettext \
  gd \
  zip \
  pdo_oci \
  pdo_dblib \
  pdo_mysql \
  pdo_pgsql \
  && docker-php-ext-install exif \
  && docker-php-ext-configure exif --enable-exif \
  && curl -s https://getcomposer.org/installer | php \
  && mv composer.phar /usr/local/bin/composer \
  && apt-get clean -y \
  && apt-get remove -y \
  && apt-get autoclean -y \
  && apt-get autoremove -y